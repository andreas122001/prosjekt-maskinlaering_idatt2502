|  name                    |    range       | description                                                            |
|  ---                     |    ---         | ---                                                                    |
|  hp                      |    [0, 100]    | the health of our current active pokemon (in buckets)                  |
|  op_hp                   |    [0, 100]    | the health of the opponent's current active pokemon                    |
|  num_fainted             |    [0, 6]      | amount of fainted pokémon on our team                                  |
|  num_fainted_op          |    [0, 6]      | amount of fainted pokémon on opponent's team                           |
|  type_advantage_1        |    [0, 2]      | damage multiplier for our first type to both the opponent's types      |
|  type_advantage_2        |    [0, 2]      | damage multiplier for our second type to both the opponent's types     |
|  is_dead                 |    [0, 1]      | if our current pokémon is dead or not                                  |
|  active_pkm              |    [0, 6]      | index of our current active pokémon                                    |
|  moves_base_power        |    [-1, 300]   | the base power of all moves (as an array)                              |
|  moves_damage_multiplier |    [0, 4]      | the damage multiplier of all moves (as an array)                       |
|  boosts                  |    [-6, 6]     | amount of each of our boosts (as an array)                             |
|  boosts_op               |    [-6, 6]     | amount of each of the opponent's boosts (as an array)                  | 