BATTLE_FORMAT = "gen8ou"

PPO_PARAMS = {
    "LEARNING_RATE":    0.01,
    "GAMMA":            0.995,
    "GAE_LAMBDA":       0.95,
    "CLIP_RANGE":       0.2,
    "BATCH_SIZE":       64,
    "ENT_COEF":         0.0,
    "VF_COEF":          0.5,
    "N_STEPS":          2048,
    "N_EPOCHS":         10,
    "TOTAL_N_STEPS":    200_000,
}

DQN_PARAMS = {
    "LEARNING_RATE": 0.001,
    "GAMMA": 0.995,
    "BUFFER_SIZE": 50000,
    "BATCH_SIZE": 64,
    "TARGET_UPDATE_INTERVAL": 10,
    "TAU": 0.05,
    "TRAIN_FREQ": 4,
    "EXPLORATION_INIT_EPS": 1,
    "EXPLORATION_FINAL_EPS": 0.01,
    "EXPLORATION_FRACTION": 0.4,
    "TOTAL_N_STEPS": 200_000,
}

SERVER_URL = "http://localhost.psim.us/"
AUTH_URL = "https://play.pokemonshowdown.com/action.php?"

USERNAME = "xXPokeAIXx"
PASSWORD = ""
