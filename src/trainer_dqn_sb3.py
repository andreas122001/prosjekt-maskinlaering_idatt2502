from environments.test_env import PokeAI
from stable_baselines3 import DQN
from players.players import RandomPlayer, MaxDamagePlayer, HeuristicPlayer
from teambuilders import RandomTeambuilder, FirstPickTeambuilder
from teams import triples, multiple
from wandb.integration.sb3 import WandbCallback
from poke_env.player_configuration import PlayerConfiguration
from dotenv import load_dotenv
from config import BATTLE_FORMAT, DQN_PARAMS
import asyncio
import wandb
import os

load_dotenv()

async def run(
        opponent_class,
        team,
        LEARNING_RATE = 0.001,
        GAMMA = 0.995,
        BUFFER_SIZE = 50000,
        BATCH_SIZE = 64,
        TARGET_UPDATE_INTERVAL = 10,
        TAU = 0.05,
        TRAIN_FREQ = 4,
        EXPLORATION_INIT_EPS = 1,
        EXPLORATION_FINAL_EPS = 0.01,
        EXPLORATION_FRACTION = 0.4,
        TOTAL_N_STEPS = 200_000,
        N_EVAL_EPISODES = 500,
        tags = [],
        do_log=False,
        name_id = 1,
        pre_trained=""):

    # Opponent, environment and agent initialization  
    opponent = opponent_class(
        battle_format=BATTLE_FORMAT,
        player_configuration=PlayerConfiguration(f"{opponent_class.__name__[:10]}_DQN{name_id}", ""),
        team=RandomTeambuilder(team)
    )
    env = PokeAI(team=FirstPickTeambuilder(team), opponent=opponent,
        player_configuration=PlayerConfiguration(f"PokeAI_DQN{name_id}", ""),
        battle_format=BATTLE_FORMAT, start_challenging=True
    )

    if pre_trained:
        agent = DQN.load(pre_trained, env)
    else:
        agent = DQN("MlpPolicy", env, verbose=1, tensorboard_log=f"runs/dqn",
            learning_rate=0.001,
            gamma=GAMMA,
            buffer_size=BUFFER_SIZE,
            batch_size=BATCH_SIZE,
            target_update_interval=TARGET_UPDATE_INTERVAL,
            tau=TAU,
            train_freq=TRAIN_FREQ,
            exploration_initial_eps=EXPLORATION_INIT_EPS,
            exploration_final_eps=EXPLORATION_FINAL_EPS,
            exploration_fraction=EXPLORATION_FRACTION,
        )

    # Set up logger (if logging)
    wb = None
    if do_log:
        try:
            wandb.login(key=os.getenv("wandb-api-key"))

            wb = wandb.init(
                project=os.getenv("wandb-project-name"),
                sync_tensorboard=True,
                tags=tags,
                config = {
                    "learning_rate": LEARNING_RATE,
                    "batch_size": BATCH_SIZE,
                    "buffer_size": BUFFER_SIZE,
                    "target_update_interval": TARGET_UPDATE_INTERVAL,
                    "exploration_initial_eps": EXPLORATION_INIT_EPS,
                    "exploration_final_eps": EXPLORATION_FINAL_EPS,
                    "exploration_fraction": EXPLORATION_FRACTION,
                    "policy": "epsilon-greedy",
                    "tau": TAU,
                    "gamma": GAMMA,
                    "train_freq": TRAIN_FREQ,
                    "total_steps": TOTAL_N_STEPS,
                    "opponent": env._opponent.__class__.__name__,
                    "environment": env.__class__.__name__,
                    "algo": "dqn-sb3",
                    "team": team,
                    "pre-trained": True if pre_trained != "" else False,
                }
            )
            wb.name = "dqn_sb3-" + wb.name
        except Exception as e:
            print("WandB failed, will no longer log: " + str(e))
            do_log = False

    print("Training...")
    # Log to WandB while training
    if do_log:
        agent.learn(TOTAL_N_STEPS, progress_bar=True, 
            callback=WandbCallback(
                gradient_save_freq=100, 
                model_save_path=f"models/{wb.name}", 
                verbose=2
            )
        )
    else: # Just train, logging only in terminal
        agent.learn(TOTAL_N_STEPS, progress_bar=True)

    print("Training complete!")
    print(f"Training win rate: {env.win_rate}")

    if do_log:
        wandb.log({"rollout/win_rate": env.win_rate})

    env.close()
    env.reset_env()

    print("Evaluating...")
    for _ in range(N_EVAL_EPISODES):
        done = False
        obs = env.reset()
        while not done:
            action = agent.predict(obs)[0]
            obs, _, done, _ = env.step(action)

    print(
        "Evaluation: %d victories out of %d episodes (%d %%)"
        % (env.n_won_battles, N_EVAL_EPISODES, env.win_rate*100)
    )

    if do_log:
        wandb.log({"rollout/eval_win_rate": env.win_rate})

    wb.finish()
    env.close()

if __name__ == "__main__":
    team = triples
    team_type = "triples"

    # Runs tests for all opponent-types (with triples as team)
    asyncio.get_event_loop().run_until_complete(
        run(
            opponent_class = RandomPlayer,
            team = team,
            tags = ["test0", f"{team_type}", "RandomPlayer"],
            do_log = True, 
            pre_trained="",
            **DQN_PARAMS
        ))
    asyncio.get_event_loop().run_until_complete(
        run(
            opponent_class = MaxDamagePlayer,
            team = team,
            tags = ["test0", f"{team_type}", "MaxDamagePlayer"],
            do_log = True,
            name_id=4,      # We have to use different names to avoid "username already taken"
            pre_trained="",
            **DQN_PARAMS
        ))
    asyncio.get_event_loop().run_until_complete(
        run(
            opponent_class = HeuristicPlayer,
            team = team,
            tags = ["test0", f"{team_type}", "HeuristicPlayer"],
            do_log = True,
            name_id=8,
            pre_trained="",
            **DQN_PARAMS
        ))
