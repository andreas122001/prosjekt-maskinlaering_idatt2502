from networks.Linear3Huber import Model
from collections import deque
import numpy as np
import random
import torch
from torch import tensor

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

class Agent:
    def __init__(self, action_space_size, observation_space_size,
            gamma=0.995,
            learning_rate=0.001,
            max_epsilon = 0.2,
            min_epsilon = 0,
            epsilon_decay = 0,
            tau=2e-3,
            model=Model,
            replay_memory_size=1000, 
            memory_batch_size=100,
            min_replay_memory_size=-1,
            update_target_rate=10,
            train_rate=1,
            model_hidden_size=32,
            use_epsilon_greedy=True 
        ):

        # Environment parameters
        self.action_space_size = action_space_size
        self.observation_space_size = observation_space_size

        # RL parameter
        self.tau = tau
        self.gamma = gamma
        self.learning_rate = learning_rate
        self.epsilon = max_epsilon
        self.max_epsilon = max_epsilon
        self.min_epsilon = min_epsilon
        self.epsilon_decay = epsilon_decay
        self.epsilon_greedy = use_epsilon_greedy

        # Neural Net
        self.model = model(self.observation_space_size, self.action_space_size, model_hidden_size).to(device)
        self.target_model = model(self.observation_space_size, self.action_space_size, model_hidden_size).to(device)
        self.update_target_every = update_target_rate
        self.train_every = train_rate
        self.target_update_counter = 0
        self.training_counter = 0
        self.optimizer = torch.optim.Adam(self.model.parameters(), self.learning_rate)
        self.model.eval()
        self.target_model.eval()

        # Replay memory
        self.replay_memory = deque(maxlen=replay_memory_size)
        self.memory_batch_size = memory_batch_size
        if (min_replay_memory_size == -1): 
            self.min_memory_size = self.memory_batch_size
        else: self.min_memory_size = min_replay_memory_size

    def append_memory(self, memory):
        self.replay_memory.append(memory)

    def predict(self, state):
        state = tensor(state).float().to(device)
        return self.model.forward(state)

    def save(self, path):
        try:
            torch.save(self.model.state_dict(), path)
        except:
            print("Could not save file")

    def load(self, path):
        try:
            self.model.load_state_dict(torch.load(path))
            self.optimizer = torch.optim.Adam(self.model.parameters(), self.learning_rate)
            self.target_model.load_state_dict(torch.load(path))
        except:
            print("Could not load file")

    def act(self, state, eval = False):
        """Uses policy to choose an action"""
        if eval:
            return self.act_epsilon_greedy(state, 0)
        elif self.epsilon_greedy:
            return self.act_epsilon_greedy(state, self.epsilon)
        else:
            return self.act_softmax(state)
            
    def act_epsilon_greedy(self, state, epsilon):
        """Uses epsilon-greedy to choose action. If eps=0.0 chooses the best action."""
        if random.random() > epsilon:
            actions = self.predict(np.array([state]))
            actions = actions.cpu().detach().numpy()[0]
            return np.argmax(actions)
        else:
            return np.random.randint(0, self.action_space_size)

    def act_softmax(self, state):
        """Uses softmax-policy to choose action"""
        prob = self.predict(np.array([state]))
        prob = torch.softmax(prob, 1)[0].cpu().detach().numpy()
        actions = np.linspace(0, len(prob)-1, num=len(prob))
        action = np.random.choice(actions, p=prob)
        return int(action)

    def update_model(self, target_model, local_model, tau):
        for target_param, local_param in zip(target_model.parameters(), local_model.parameters()):
            target_param.data.copy_(tau*local_param.data + (1.0-tau)*target_param.data)

    def update_epsilon(self):
        epsilon = self.epsilon * (1-self.epsilon_decay)
        self.epsilon = max(self.min_epsilon, epsilon)

    def reset_epsilon(self):
        self.epsilon = self.max_epsilon

    def step(self, state, action, reward, next_state, done):
        self.append_memory((state, action, reward, next_state, done))
        self.training_counter += 1

        if self.training_counter >= self.train_every and len(self.replay_memory) >= self.min_memory_size:
            self.update()
            self.training_counter = 0

        if self.epsilon_greedy and done:
            self.update_epsilon()

    def update(self):

        # Get a batch of random samples from memory replay table
        batch = np.array(random.sample(self.replay_memory, self.memory_batch_size), dtype=object)

        # Get current states from memory, predict q-values for current states
        current_states = tensor(np.stack(batch[:,0])).float().to(device)
        current_qs_list = self.model.forward(current_states).cpu().detach().numpy()

        # Get next states from memory, predict q-values for next states using target model
        next_states = tensor(np.stack(batch[:,3])).float().to(device)
        future_qs_list = self.target_model.forward(next_states).cpu().detach().numpy()

        X = np.array([])
        y = np.array([])

        for index, (current_state, action, reward, next_state, done) in enumerate(batch):

            # If not a terminal state, get new q from future states, otherwise set it to 0
            if not done:
                max_future_q = np.max(future_qs_list[index])
                new_q = reward + self.gamma * max_future_q
            else:
                new_q = reward

            # Update Q value for given state
            current_qs = current_qs_list[index]
            current_qs[int(action)] = new_q

            # And append to our training data
            X = np.append(X, current_state)
            y = np.append(y, current_qs)

        # Prepare train-data
        X = tensor(X).reshape(batch.shape[0], -1).float().to(device)
        y = tensor(y).reshape(batch.shape[0], -1).float().to(device)

        # Fit on all samples as one batch
        self.model.train()
        self.optimizer.zero_grad()
        loss = self.model.loss(X, y)
        loss.backward()
        self.optimizer.step()
        self.model.eval()

        # Update target network counter every episode
        if done:
            self.target_update_counter += 1

        # If counter reaches set value, update target network with weights of main network
        if self.target_update_counter >= self.update_target_every:
            self.update_model(self.target_model, self.model, self.tau)
            self.target_update_counter = 0


# Testing and debugging
# state_size=2
# action_size=5
# agent = Agent(observation_space_size=state_size, action_space_size=action_size, min_replay_memory_size=0, memory_batch_size=4, learning_rate=0.01)

# (state, action, reward, next_state, done) = (np.array([1., 1.]), 0, 1, np.array([1., 1.]), 0)

# agent.append_memory((state, 0, -1000, next_state, done))
# agent.append_memory((state, 1,  1000, next_state, done))
# agent.append_memory((state, 2, -1000, next_state, done))
# agent.append_memory((state, 3, -1000, next_state, done))
# agent.append_memory((state, 4, -1000, next_state, done))

# for i in range(100):
#     agent.train()

# action = agent.act(state)
# # pred = agent.predict([2.,2.])
# print("action:", action)
