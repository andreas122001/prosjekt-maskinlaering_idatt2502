from poke_env.teambuilder import Teambuilder
from numpy.random import choice

class RandomTeambuilder(Teambuilder):
    def __init__(self, teams):
        self.teams = [self.join_team(self.parse_showdown_team(team)) for team in teams]
    
    def yield_team(self):
        return choice(self.teams)

class FirstPickTeambuilder(Teambuilder):
    def __init__(self, teams):
        self.teams = [self.join_team(self.parse_showdown_team(team)) for team in teams]
    
    def yield_team(self):
        return self.teams[0]