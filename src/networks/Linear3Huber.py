from torch import nn

class Model(nn.Module):

    def __init__(self, in_size, out_size, hidden_size = 32):
        super(Model, self).__init__()
        self.in_size = in_size
        self.out_size = out_size
        self.hidden_size = hidden_size

        self.logits = nn.Sequential(

            # Layer 1
            nn.Linear(self.in_size, self.hidden_size),
            nn.ReLU(),
            nn.Dropout(0.2),

            # Layer 2
            nn.Linear(self.hidden_size, self.hidden_size),
            nn.ReLU(),
            nn.Dropout(0.2),
        
            # Layer 3
            nn.Linear(self.hidden_size, self.out_size),
            nn.ReLU(),
        )

    def forward(self, x):
        return self.logits(x)
    
    def loss(self, x, y):
        return nn.functional.huber_loss(self.forward(x), y)

    