from poke_env.player import Player, MaxBasePowerPlayer, SimpleHeuristicsPlayer, RandomPlayer
import numpy as np

class SimpleHeuristicPlayer(Player):
    def choose_move(self, battle):
        # If the player can attack, it will
        if battle.available_moves:
            best_move = battle.available_moves[0]

            boost_moves = list(filter(lambda move: move.boosts, battle.available_moves))

            # if we have a lot of health, we use a boosting move
            if battle.active_pokemon.current_hp_fraction > 0.8 and 6 not in battle.active_pokemon.boosts.values() and boost_moves:
                best_move = np.random.choice(boost_moves)

            # Else we attack!
            else:
                # Finds the move with most damage overall (base, type-multiplier and STAB)
                best_move = max(battle.available_moves, key=lambda move: 
                                (move.type.damage_multiplier(
                                    battle.opponent_active_pokemon.type_1, battle.opponent_active_pokemon.type_2 # multiplier
                                ) * (move.base_power                                                        # base power
                                    + move.base_power * int(battle.active_pokemon.type_1 == move.type)*0.5 # STAB
                                    + move.base_power * int(battle.active_pokemon.type_2 == move.type)*0.5 # STAB
                                    ))
                            )

                # If the type-advantage is below 1 (bad type), we switch pokemon
                if best_move.type.damage_multiplier(battle.opponent_active_pokemon.type_1,
                    battle.opponent_active_pokemon.type_2) < 1:
                    if battle.available_switches:
                        self.create_order(battle.available_switches[0])
            return self.create_order(best_move)

        # If no attack is available, a random switch will be made
        else:
            return self.choose_random_move(battle)

class MaxDamagePlayer(MaxBasePowerPlayer):
    pass

class HeuristicPlayer(SimpleHeuristicsPlayer):
    pass

class RandomPlayer(RandomPlayer):
    pass
