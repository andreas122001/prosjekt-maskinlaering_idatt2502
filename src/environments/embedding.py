from poke_env.environment.abstract_battle import AbstractBattle
import numpy as np
from gym.spaces import Box

def embed(battle: AbstractBattle):
    mon = battle.active_pokemon
    op_mon = battle.opponent_active_pokemon

    num_fainted = len([mon for mon in battle.team.values() if mon.fainted]) / 6
    num_fainted_op = (
        len([mon for mon in battle.opponent_team.values() if mon.fainted]) / 6
    )

    boosts = list(mon.boosts.values())
    op_boosts = list(op_mon.boosts.values())

    health_bins = np.linspace(0,1,5)

    hp = np.digitize(mon.current_hp_fraction, health_bins) / 5
    op_hp = np.digitize(op_mon.current_hp_fraction, health_bins) / 5

    isDead = np.array([0, 0])
    if (mon.current_hp_fraction == 0):
        isDead[1] = 1
    else: 
        isDead[0] = 1

    team = list(battle.team.values())
    active_pkm = np.zeros(6)

    for i in range(len(team)):
        if team[i].active:
            active_pkm[0] = 1

    type_advantage_1 = mon.type_1.damage_multiplier(
                    op_mon.type_1,
                    op_mon.type_2,
                )

    type_advantage_2 = type_advantage_1
    if (mon.type_2):
        type_advantage_2 = mon.type_2.damage_multiplier(
                        op_mon.type_1,
                        op_mon.type_2,
                    )

    moves_base_power = -np.ones(4)
    moves_dmg_multiplier = np.ones(4)
    for i, move in enumerate(battle.available_moves):
        if move.current_pp:
            moves_base_power[i] = (
                move.base_power / 100
            )
            if move.type:
                moves_dmg_multiplier[i] = move.type.damage_multiplier(
                    battle.opponent_active_pokemon.type_1,
                    battle.opponent_active_pokemon.type_2,
                )

    final = np.concatenate(
        [
            [hp, op_hp, num_fainted, num_fainted_op, type_advantage_1, type_advantage_2],
            isDead,
            active_pkm,
            moves_base_power, moves_dmg_multiplier, 
            boosts, op_boosts
        ]
    )

    final = np.float32(final)

    return final

def describe():
    low =  [0,0,0,0,0,0, 0,0, 0,0,0,0,0,0, -1,-1,-1,-1, 0,0,0,0, -6,-6,-6,-6,-6,-6,-6, -6,-6,-6,-6,-6,-6,-6]
    high = [1,1,1,1,2,2, 1,1, 1,1,1,1,1,1,  3,3,3,3,    4,4,4,4,  6,6,6,6,6,6,6,        6,6,6,6,6,6,6]
    return Box(
        np.array(low, dtype=np.float32),
        np.array(high, dtype=np.float32),
        dtype=np.float32,
    )