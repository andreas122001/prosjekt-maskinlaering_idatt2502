from poke_env.player import Gen4EnvSinglePlayer
from poke_env.environment.abstract_battle import AbstractBattle
from . import embedding

class PokeAI(Gen4EnvSinglePlayer):
    def calc_reward(self, last_battle: AbstractBattle, current_battle: AbstractBattle) -> float:
        return self.reward_computing_helper(current_battle) 
           
    def embed_battle(self, battle: AbstractBattle):
        return embedding.embed(battle)

    def describe_embedding(self):
        return embedding.describe()
