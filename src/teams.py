triples = ["""
Bulbasaur  
Ability: Overgrow  
EVs: 80 HP / 64 Atk / 100 Def / 100 SpA / 96 SpD / 68 Spe  
Gentle Nature  
IVs: 0 Spe  
- Tackle  
- Growth  
- Growl 
- Mega Drain  

Charmander 
Ability: Blaze  
EVs: 252 HP / 252 Atk / 4 SpA  
Lonely Nature  
- Swords Dance  
- Quick Attack  
- Ember  
- Scratch 

Squirtle  
Ability: Torrent  
EVs: 64 HP / 44 Atk / 44 Def / 48 SpA / 52 SpD / 52 Spe  
Bold Nature  
- Protect  
- Water Gun  
- Shell Smash
- Tackle  
"""]

sixes = ["""
Charmander  
Ability: Solar Power
EVs: 252 Atk / 4 SpD / 252 Spe  
Adamant Nature  
- Celebrate  
- Dragon Dance  
- Fire Punch  
- Brick Break

Squirtle  
Ability: Torrent  
EVs: 4 Atk / 252 SpA / 252 Spe  
Rash Nature  
- Shell Smash  
- Protect  
- Scald  
- Ice Punch

Bulbasaur  
Ability: Overgrow  
EVs: 252 HP / 4 Atk / 252 SpD  
Sassy Nature  
- Giga Drain  
- Rest  
- Sleep Talk  
- Double-Edge

Pikachu  
Ability: Static  
EVs: 252 Atk / 4 SpA / 252 Spe  
Hasty Nature  
- Surf  
- Brick Break  
- Thunderbolt  
- Knock Off

Beldum  
Ability: Clear Body  
EVs: 252 HP / 252 Atk / 4 SpA  
Brave Nature  
- Iron Defense  
- Iron Head  
- Steel Beam  
- Zen Headbutt

Drifloon  
Ability: Aftermath  
EVs: 248 HP / 252 Atk / 8 SpA  
Lonely Nature  
- Thunderbolt  
- Explosion  
- Pain Split  
- Acrobatics
"""
]


multiple = [
"""
Regieleki @ Heavy-Duty Boots  
Ability: Transistor  
EVs: 80 HP / 252 SpA / 176 Spe  
Timid Nature  
- Volt Switch  
- Electro Ball  
- Rapid Spin  
- Ancient Power  

Ferrothorn @ Rocky Helmet  
Ability: Iron Barbs  
EVs: 252 HP / 252 Def / 4 SpD  
Relaxed Nature  
IVs: 0 Spe  
- Knock Off  
- Spikes  
- Leech Seed  
- Gyro Ball  

Garchomp @ Leftovers  
Ability: Rough Skin  
EVs: 252 Atk / 4 SpD / 252 Spe  
Jolly Nature  
- Swords Dance  
- Earthquake  
- Fire Fang  
- Scale Shot  

Landorus-Therian (M) @ Leftovers  
Ability: Intimidate  
EVs: 248 HP / 252 SpD / 8 Spe  
Careful Nature  
- Stealth Rock  
- Earthquake  
- U-turn  
- Toxic  

Pelipper @ Choice Specs  
Ability: Drizzle  
EVs: 4 Def / 252 SpA / 252 Spe  
Modest Nature  
- Hydro Pump  
- U-turn  
- Hurricane  
- Weather Ball  

Weavile @ Heavy-Duty Boots  
Ability: Pressure  
EVs: 252 Atk / 4 SpD / 252 Spe  
Jolly Nature  
- Swords Dance  
- Triple Axel  
- Knock Off  
- Ice Shard
""",


"""
Dragapult @ Choice Specs  
Ability: Infiltrator  
EVs: 252 SpA / 4 SpD / 252 Spe  
Modest Nature  
- Shadow Ball  
- Draco Meteor  
- Flamethrower  
- U-turn  

Urshifu-Rapid-Strike @ Choice Band  
Ability: Unseen Fist  
EVs: 252 Atk / 4 SpD / 252 Spe  
Jolly Nature  
- Close Combat  
- Surging Strikes  
- U-turn  
- Aqua Jet  

Tapu Lele @ Choice Scarf  
Ability: Psychic Surge  
EVs: 252 SpA / 4 SpD / 252 Spe  
Modest Nature  
IVs: 0 Atk  
- Moonblast  
- Psyshock  
- Thunderbolt  
- Future Sight  

Landorus-Therian (M) @ Leftovers  
Ability: Intimidate  
EVs: 248 HP / 204 SpD / 56 Spe  
Careful Nature  
- Earthquake  
- Knock Off  
- U-turn  
- Stealth Rock  

Zapdos @ Heavy-Duty Boots  
Ability: Static  
EVs: 4 HP / 252 SpA / 252 Spe  
Timid Nature  
IVs: 0 Atk  
- Volt Switch  
- Hurricane  
- Heat Wave  
- Defog  

Melmetal @ Leftovers  
Ability: Iron Fist  
EVs: 252 Atk / 196 SpD / 60 Spe  
Adamant Nature  
- Double Iron Bash  
- Earthquake  
- Toxic  
- Protect  
""",

"""
Slowking-Galar @ Assault Vest  
Ability: Regenerator  
EVs: 248 HP / 16 Def / 216 SpA / 28 SpD  
Modest Nature  
IVs: 0 Atk  
- Sludge Bomb  
- Psyshock  
- Flamethrower  
- Hydro Pump  

Sharpedo @ Life Orb  
Ability: Speed Boost  
EVs: 252 SpA / 4 SpD / 252 Spe  
Modest Nature  
IVs: 0 Atk  
- Protect  
- Destiny Bond  
- Dark Pulse  
- Hydro Pump  

Landorus-Therian (M) @ Leftovers  
Ability: Intimidate  
EVs: 252 HP / 248 SpD / 8 Spe  
Careful Nature  
- Defog  
- Earthquake  
- U-turn  
- Knock Off  

Heatran @ Air Balloon  
Ability: Flame Body  
EVs: 248 HP / 220 Def / 40 Spe  
Bold Nature  
IVs: 0 Atk  
- Magma Storm  
- Stealth Rock  
- Toxic  
- Earth Power  

Celesteela @ Leftovers  
Ability: Beast Boost  
EVs: 248 HP / 80 Def / 128 SpD / 52 Spe  
Calm Nature  
IVs: 0 Atk  
- Substitute  
- Meteor Beam  
- Flash Cannon  
- Leech Seed  

Rillaboom @ Choice Band  
Ability: Grassy Surge  
EVs: 252 Atk / 4 Def / 252 Spe  
Adamant Nature  
- Grassy Glide  
- Superpower  
- Wood Hammer  
- U-turn  
""",


"""
Glastrier @ Red Card  
Ability: Chilling Neigh  
EVs: 176 HP / 252 Atk / 80 Spe  
Adamant Nature  
- Swords Dance  
- Icicle Spear  
- High Horsepower  
- Megahorn  

Tapu Fini @ Leftovers  
Ability: Misty Surge  
EVs: 248 HP / 192 Def / 68 Spe  
Bold Nature  
- Calm Mind  
- Taunt  
- Scald  
- Draining Kiss  

Tornadus-Therian (M) @ Heavy-Duty Boots  
Ability: Regenerator  
EVs: 252 HP / 88 Def / 168 Spe  
Timid Nature  
- Heat Wave  
- Knock Off  
- U-turn  
- Defog  

Ferrothorn @ Leftovers  
Ability: Iron Barbs  
EVs: 252 HP / 88 Def / 168 SpD  
Careful Nature  
- Spikes  
- Knock Off  
- Leech Seed  
- Power Whip  

Garchomp @ Leftovers  
Ability: Rough Skin  
EVs: 252 HP / 232 SpD / 24 Spe  
Careful Nature  
- Stealth Rock  
- Toxic  
- Protect  
- Earthquake  

Dragonite @ Heavy-Duty Boots  
Ability: Multiscale  
EVs: 252 Atk / 4 SpD / 252 Spe  
Jolly Nature  
- Dragon Dance  
- Earthquake  
- Ice Punch  
- Roost  
""",


"""
Blissey (F) @ Heavy-Duty Boots  
Ability: Natural Cure  
EVs: 252 HP / 252 Def / 4 SpD  
Bold Nature  
IVs: 0 Atk  
- Calm Mind  
- Stored Power  
- Flamethrower  
- Soft-Boiled  

Blaziken @ Leftovers  
Ability: Speed Boost  
EVs: 120 HP / 244 Atk / 144 Spe  
Adamant Nature  
- Swords Dance  
- Protect  
- Flare Blitz  
- Close Combat  

Toxapex @ Choice Specs  
Ability: Regenerator  
EVs: 152 HP / 252 SpA / 104 Spe  
Modest Nature  
IVs: 0 Atk  
- Sludge Bomb  
- Scald  
- Hydro Pump  
- Ice Beam  

Landorus-Therian (M) @ Leftovers  
Ability: Intimidate  
EVs: 248 HP / 236 SpD / 24 Spe  
Careful Nature  
- Defog  
- Earthquake  
- U-turn  
- Toxic  

Skarmory @ Rocky Helmet  
Ability: Sturdy  
EVs: 248 HP / 240 Def / 20 Spe  
Bold Nature  
IVs: 0 Atk  
- Stealth Rock  
- Iron Defense  
- Body Press  
- Roost  

Dragapult @ Choice Specs  
Ability: Infiltrator  
EVs: 252 SpA / 4 SpD / 252 Spe  
Timid Nature  
- Shadow Ball  
- Draco Meteor  
- Flamethrower  
- U-turn  
""",


"""
Lumberjack (Diggersby) (M) @ Life Orb  
Ability: Huge Power  
Shiny: Yes  
EVs: 4 HP / 252 Atk / 252 Spe  
Adamant Nature  
- Mega Kick  
- Thunder Punch  
- Swords Dance  
- Quick Attack  

Wyatt (Shuckle) (M) @ Mental Herb  
Ability: Sturdy  
Shiny: Yes  
EVs: 248 HP / 136 Def / 124 SpD  
Sassy Nature  
IVs: 0 Atk  
- Stealth Rock  
- Sticky Web  
- Final Gambit  
- Encore  

Leviathan (Dragonite) (M) @ Heavy-Duty Boots  
Ability: Multiscale  
Shiny: Yes  
EVs: 44 HP / 212 Atk / 252 Spe  
Jolly Nature  
- Roost  
- Ice Punch  
- Earthquake  
- Dragon Dance  

Shellshock (Blastoise) (M) @ White Herb  
Ability: Torrent  
Shiny: Yes  
EVs: 252 SpA / 4 SpD / 252 Spe  
Modest Nature  
IVs: 0 Atk  
- Shell Smash  
- Surf  
- Focus Blast  
- Terrain Pulse  

Quintessa (Tapu Koko) @ Terrain Extender  
Ability: Electric Surge  
Shiny: Yes  
EVs: 252 SpA / 4 SpD / 252 Spe  
Timid Nature  
- Thunderbolt  
- U-turn  
- Dazzling Gleam  
- Roost  

Achilles (Aegislash) (M) @ Choice Specs  
Ability: Stance Change  
Shiny: Yes  
EVs: 252 SpA / 4 SpD / 252 Spe  
Modest Nature  
- Shadow Ball  
- Flash Cannon  
- Shadow Sneak  
- Steel Beam  
""",


"""
Pelipper @ Damp Rock  
Ability: Drizzle  
EVs: 248 HP / 252 Def / 8 SpD  
Bold Nature  
- Defog  
- U-turn  
- Scald  
- Roost  

Seismitoad @ Life Orb  
Ability: Swift Swim  
EVs: 4 Atk / 252 SpA / 252 Spe  
Rash Nature  
- Earth Power  
- Weather Ball  
- Power Whip  
- Ice Punch  

Barraskewda @ Choice Band  
Ability: Swift Swim  
EVs: 252 Atk / 4 SpD / 252 Spe  
Adamant Nature  
- Flip Turn  
- Close Combat  
- Liquidation  
- Psychic Fangs  

Zapdos @ Heavy-Duty Boots  
Ability: Static  
EVs: 4 Def / 252 SpA / 252 Spe  
Timid Nature  
IVs: 0 Atk  
- Hurricane  
- Weather Ball  
- Thunder  
- Roost  

Ferrothorn @ Custap Berry  
Ability: Iron Barbs  
EVs: 252 HP / 88 Def / 168 SpD  
Impish Nature  
- Stealth Rock  
- Body Press  
- Power Whip  
- Explosion  

Blastoise @ Life Orb  
Ability: Torrent  
EVs: 252 SpA / 4 SpD / 252 Spe  
Timid Nature  
IVs: 0 Atk  
- Hydro Pump  
- Focus Blast  
- Hyper Beam  
- Shell Smash  
""",


"""
Blastoise @ White Herb  
Ability: Torrent  
EVs: 4 Def / 252 SpA / 252 Spe  
Modest Nature  
IVs: 0 Atk  
- Shell Smash  
- Ice Beam  
- Hydro Pump  
- Aura Sphere

Regieleki @ Light Clay  
Ability: Transistor  
EVs: 4 HP / 252 Atk / 252 Spe  
Adamant Nature  
- Reflect  
- Light Screen  
- Rapid Spin  
- Explosion  

Zeraora @ Heavy-Duty Boots  
Ability: Volt Absorb  
EVs: 252 Atk / 4 SpD / 252 Spe  
Jolly Nature  
- Plasma Fists  
- Close Combat  
- Knock Off  
- Bulk Up  

Landorus-Therian (M) @ Focus Sash  
Ability: Intimidate  
EVs: 252 Atk / 4 Def / 252 Spe  
Jolly Nature  
- Swords Dance  
- Substitute  
- Earthquake  
- Stone Edge  

Dragapult @ Dragon Fang  
Ability: Clear Body  
EVs: 112 HP / 252 Atk / 12 SpD / 132 Spe  
Naughty Nature  
- Dragon Dance  
- Dragon Darts  
- Steel Wing  
- Flamethrower  

Heatran @ Air Balloon  
Ability: Flame Body  
EVs: 248 HP / 248 Def / 12 Spe  
Bold Nature  
IVs: 0 Atk  
- Magma Storm  
- Stealth Rock  
- Earth Power  
- Toxic  
""",

"""
Flygon @ Choice Specs  
Ability: Levitate  
EVs: 4 Def / 252 SpA / 252 Spe  
Modest Nature  
- Boomburst  
- U-turn  
- Fire Blast  
- Earth Power  

Celesteela @ Power Herb  
Ability: Beast Boost  
EVs: 252 SpA / 4 SpD / 252 Spe  
Modest Nature  
IVs: 0 Atk  
- Autotomize  
- Fire Blast  
- Air Slash  
- Meteor Beam  

Clefable @ Sticky Barb  
Ability: Magic Guard  
EVs: 252 HP / 4 Def / 252 SpD  
Calm Nature  
IVs: 0 Atk  
- Trick  
- Stealth Rock  
- Soft-Boiled  
- Moonblast  

Zapdos-Galar @ Choice Band  
Ability: Defiant  
EVs: 252 Atk / 4 SpD / 252 Spe  
Jolly Nature  
- U-turn  
- Close Combat  
- Brave Bird  
- Thunderous Kick  

Toxapex @ Rocky Helmet  
Ability: Regenerator  
EVs: 252 HP / 252 Def / 4 SpD  
Impish Nature  
- Toxic Spikes  
- Toxic  
- Knock Off  
- Recover  

Dragapult @ Power Herb  
Ability: Clear Body  
EVs: 252 Atk / 4 SpD / 252 Spe  
Adamant Nature  
- Dragon Dance  
- Dragon Darts  
- Phantom Force  
- Sucker Punch  
"""
]