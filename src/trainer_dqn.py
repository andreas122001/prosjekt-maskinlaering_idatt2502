from tqdm import tqdm
import wandb
import os
from dotenv import load_dotenv
from agents.DDQNAgent import Agent
from players.players import SimpleHeuristicPlayer, RandomPlayer, MaxDamagePlayer, HeuristicPlayer
from teambuilders import RandomTeambuilder, FirstPickTeambuilder
from teams import sixes, triples
import asyncio
import numpy as np
from environments.test_env import PokeAI
from config import BATTLE_FORMAT

# NOTE: Not used anymore

# Load environment variables from .env-file
load_dotenv()

# Hyperparameters
EPISODES = 2000
LEARNING_RATE = 0.0001
GAMMA = 0.995
REPLAY_MEMORY_SIZE = 50000
MEMORY_BATCH_SIZE = 64
TARGET_UPDATE_RATE = 5
TAU = 0.05
NETWORK_HIDDEN_SIZE = 32
UPDATE_RATE = 1
MAX_EPSILON = 1
MIN_EPSILON = 0.01
EPSILON_DECAY = 0.005
USE_EPSILON_GREEDY = True

# Opponent and team
OPPONENT = HeuristicPlayer
TEAM = triples

N_EVAL_EPISODES = 500

TAGS = ["test1"]

async def run(
    do_log = False,
    pre_trained_model = "",
):
    """Train an agent on a environment"""
    opponent = OPPONENT(
        battle_format=BATTLE_FORMAT,
        team=RandomTeambuilder(TEAM)
    )
    env = PokeAI(team=FirstPickTeambuilder(TEAM), opponent=opponent,
        battle_format=BATTLE_FORMAT, start_challenging=True)

    wb = None
    if do_log:
        try:
            wandb.login(key=os.getenv("wandb-api-key"))

            wb = wandb.init(
                    project=os.getenv("wandb-project-name"),
                    tags=TAGS,
                    config = {
                        "learning_rate": LEARNING_RATE,
                        "episodes": EPISODES,
                        "memory_batch_size": MEMORY_BATCH_SIZE,
                        "replay_memory_size": REPLAY_MEMORY_SIZE,
                        "target_update_rate": TARGET_UPDATE_RATE,
                        "max_epsilon": MAX_EPSILON if USE_EPSILON_GREEDY else -1,
                        "min_epsilon": MIN_EPSILON if USE_EPSILON_GREEDY else -1,
                        "epsilon_decay": EPSILON_DECAY if USE_EPSILON_GREEDY else -1,
                        "policy": "epsilon-greedy" if USE_EPSILON_GREEDY else "softmax",
                        "tau": TAU,
                        "network_hidden_size": NETWORK_HIDDEN_SIZE,
                        "gamma": GAMMA,
                        "update_rate": UPDATE_RATE,
                        "opponent": env._opponent.__class__.__name__,
                        "team": TEAM,
                        "environment": env.__class__.__name__,
                        "algo": "ddqn"
                    }
                )
        except:
            print("WandB failed, will no longer log")
            do_log = False

    agent = Agent(action_space_size=env.action_space_size(), observation_space_size=env.observation_space.shape[0],
        gamma=GAMMA,
        learning_rate=LEARNING_RATE,
        max_epsilon = MAX_EPSILON,
        min_epsilon = MIN_EPSILON,
        epsilon_decay = EPSILON_DECAY,
        replay_memory_size=REPLAY_MEMORY_SIZE,
        memory_batch_size=MEMORY_BATCH_SIZE,
        update_target_rate=TARGET_UPDATE_RATE,
        train_rate=UPDATE_RATE,
        tau=TAU,
        model_hidden_size=NETWORK_HIDDEN_SIZE,
        use_epsilon_greedy=USE_EPSILON_GREEDY
    )

    print("Training...")
    ep_rew = []
    action_counts = np.zeros(env.action_space_size())
    for episode in tqdm(range(EPISODES), unit="battles"):
        state = env.reset()

        done = False
        ep_reward = 0
        while not done:
            action = agent.act(state)

            next_state, reward, done, info = env.step(action)

            agent.step(state, action, reward, next_state, done)

            state = next_state
            ep_reward += reward

            action_counts[action] += 1

        # After episode
        if do_log:
            wandb.log({
                "rollout/episode": episode,
                "rollout/ep_rew_mean": ep_reward,
                "rollout/num_memories": len(agent.replay_memory),
                "rollout/win_rate": env.win_rate,
                "rollout/exploration_rate": agent.epsilon
            })

    if do_log:
        data = [[count, action] for count, (action) in enumerate(action_counts)]
        table = wandb.Table(data=data, columns=["action", "count"])
        wandb.log({
            "action_counts": wandb.plot.bar(table, value="count",
            label="action", title="Action counts")
        })     

    agent.save(f"models/dqn-{wb.name}")

    print("Training complete!")
    print(f"Training win rate: {env.win_rate}")

    env.close()
    env.reset_env()

    print("Evaluating...")
    for _ in range(N_EVAL_EPISODES):
        done = False
        obs = env.reset()
        while not done:
            action = agent.act(obs, eval=True)
            obs, _, done, _ = env.step(action)

    print(
        "Evaluation: %d victories out of %d episodes (%d %%)"
        % (env.n_won_battles, N_EVAL_EPISODES, env.win_rate*100)
    )

    if do_log:
        wandb.log({"rollout/eval_win_rate": env.win_rate})

asyncio.get_event_loop().run_until_complete(
    run(
        do_log=True
        )
)

