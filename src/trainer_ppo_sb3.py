from environments.test_env import PokeAI
from stable_baselines3 import PPO
from poke_env.player import Player
from players.players import RandomPlayer, MaxDamagePlayer, HeuristicPlayer
from teambuilders import RandomTeambuilder, FirstPickTeambuilder
from teams import triples, multiple
from wandb.integration.sb3 import WandbCallback
from poke_env.player_configuration import PlayerConfiguration
from dotenv import load_dotenv
from config import BATTLE_FORMAT, PPO_PARAMS
import wandb
import asyncio
import os

load_dotenv()

async def run(
        opponent_class: Player,
        team,
        LEARNING_RATE = 0.01,
        GAMMA = 0.995,
        GAE_LAMBDA = 0.95,
        CLIP_RANGE = 0.2,
        BATCH_SIZE = 64,
        ENT_COEF = 0.0,
        VF_COEF = 0.5,
        N_STEPS = 2048,
        N_EPOCHS = 10,
        TOTAL_N_STEPS = 200_000,
        N_EVAL_EPISODES = 500,
        tags = [],
        do_log=False, 
        name_id = 1,        # if you want to run multiple agents at once, use different names
        pre_trained=""):

    # Opponent, environment and agent initialization  
    opponent = opponent_class(
        battle_format=BATTLE_FORMAT,
        player_configuration=PlayerConfiguration(f"{opponent_class.__name__[:10]}_PPO{name_id}", ""),
        team=RandomTeambuilder(team)
    )
    env = PokeAI(team=FirstPickTeambuilder(team), opponent=opponent,
        player_configuration=PlayerConfiguration(f"PokeAI_PPO{name_id}", ""),
        battle_format=BATTLE_FORMAT, start_challenging=True
    )

    # Model loading
    if pre_trained:
        agent = PPO.load(pre_trained, env)
    else:
        agent = PPO("MlpPolicy", env, verbose=1, tensorboard_log=f"runs/ppo",
            learning_rate=0.001,
            gamma=GAMMA,
            gae_lambda=GAE_LAMBDA,
            clip_range=CLIP_RANGE
        )

    # Set up logger (if logging)
    wb = None
    if do_log:
        try:
            wandb.login(key=os.getenv("wandb-api-key"))

            wb = wandb.init(
                project=os.getenv("wandb-project-name"),
                sync_tensorboard=True,
                tags=tags,
                config = {
                    "learning_rate": LEARNING_RATE,
                    "gamma": GAMMA,
                    "gae_lambda": GAE_LAMBDA,
                    "clip_range": CLIP_RANGE,
                    "total_steps": TOTAL_N_STEPS,
                    "batch_size": BATCH_SIZE,
                    "ent_coef": ENT_COEF,
                    "cf_coef": VF_COEF,
                    "n_steps": N_STEPS,
                    "n_epochs": N_EPOCHS,
                    "opponent": env._opponent.__class__.__name__,
                    "environment": env.__class__.__name__,
                    "algo": "ppo-sb3",
                    "team": team,
                    "pre-trained": True if pre_trained != "" else False,
                }
            )
            wb.name = "ppo_sb3-" + wb.name
        except Exception as e:
            print("WandB failed, will no longer log: " + str(e))
            do_log = False

    print("Training...")
    # Log to WandB while training
    if do_log:
        agent.learn(TOTAL_N_STEPS, progress_bar=True, 
            callback=WandbCallback(
                gradient_save_freq=100, 
                model_save_path=f"models/{wb.name}", 
                verbose=2
            )
        )
    else: # Just train, logging only in terminal
        agent.learn(TOTAL_N_STEPS, progress_bar=True)

    print("Training complete!")
    print(f"Training win rate: {env.win_rate}")

    if do_log:
        wandb.log({"rollout/win_rate": env.win_rate})

    agent.save(f"models/ppo_sb3-{wb.name}")

    env.close()
    env.reset_env()

    print("Evaluating...")
    for _ in range(N_EVAL_EPISODES):
        done = False
        obs = env.reset()
        while not done:
            action = agent.predict(obs)[0]
            obs, _, done, _ = env.step(action)

    print(
        "Evaluation: %d victories out of %d episodes (%d %%)"
        % (env.n_won_battles, N_EVAL_EPISODES, env.win_rate*100)
    )

    if do_log:
        wandb.log({"rollout/eval_win_rate": env.win_rate})

    wb.finish()
    env.close()

if __name__ == "__main__":
    team = triples
    team_type = "triples" # this is just for tags

    # Runs tests for all opponent-types (with triples as team)
    asyncio.get_event_loop().run_until_complete(
        run(
            opponent_class = RandomPlayer,
            team = team,
            tags = ["test0", f"{team_type}", "RandomPlayer"],
            do_log = True, 
            pre_trained="",      
            **PPO_PARAMS
        ))
    asyncio.get_event_loop().run_until_complete(
        run(
            opponent_class = MaxDamagePlayer,
            team = team,
            tags = ["test0", f"{team_type}", "MaxDamagePlayer"],
            do_log = True, 
            name_id=5,
            pre_trained="",
            **PPO_PARAMS
        ))
    asyncio.get_event_loop().run_until_complete(
        run(
            opponent_class = HeuristicPlayer,
            team = team,
            tags = ["test0", f"{team_type}", "HeuristicPlayer"],
            do_log = True, 
            name_id=10,
            pre_trained="",
            **PPO_PARAMS
        ))
