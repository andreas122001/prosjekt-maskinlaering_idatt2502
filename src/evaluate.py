from poke_env.player_configuration import PlayerConfiguration
from teambuilders import RandomTeambuilder, FirstPickTeambuilder
from config import BATTLE_FORMAT
from environments.test_env import PokeAI
import asyncio


async def run(agent_class, opponent_class, team, model_path, n_runs = 1, name_id=100):
    # Opponent, environment and agent initialization  
    opponent = opponent_class(
        battle_format=BATTLE_FORMAT,
        player_configuration=PlayerConfiguration("Heuristic_PPO"[:10]+f"{name_id}", ""),
        team=RandomTeambuilder(team)
    )
    env = PokeAI(team=FirstPickTeambuilder(team), opponent=opponent,
        player_configuration=PlayerConfiguration("PokeAI_PPO"[:10]+f"{name_id}", ""),
        battle_format=BATTLE_FORMAT, start_challenging=False
    )

    agent = agent_class.load(model_path, env)

    env.start_challenging(n_runs)
    print("Evaluating...")
    for _ in range(n_runs):
        done = False
        obs = env.reset()
        while not done:
            action = agent.predict(obs)[0]
            obs, _, done, _ = env.step(action)

    print(
        "Evaluation: %d victories out of %d episodes (%d %%)"
        % (env.n_won_battles, n_runs, env.win_rate*100)
    )

if __name__ == "__main__":
    from stable_baselines3 import PPO, DQN
    from players.players import HeuristicPlayer
    from teams import triples, multiple

    path = "models\ppo_sb3-ethereal-snowflake-185\model.zip"
    asyncio.get_event_loop().run_until_complete(
        run(
            PPO, HeuristicPlayer, multiple, path, 10
        )
    )
