from src.trainer_ppo_sb3 import run as PPO_run
from src.trainer_dqn_sb3 import run as DQN_run
from src.players.players import HeuristicPlayer, MaxDamagePlayer, RandomPlayer
from config import PPO_PARAMS, DQN_PARAMS
from src.teams import triples, sixes, multiple
import asyncio

# This file provides an example of running the 'trainers'.

# Run the DQN-trainer
asyncio.get_event_loop().run_until_complete( # Wrap in asyncio (required)
    DQN_run(
        opponent_class = HeuristicPlayer,
        team = triples,                             # array of teams, where the first will be used by the agent
        tags = ["myTest", "triples"],               # Tags for WandB
        do_log = True, 
        pre_trained="",
        **DQN_PARAMS                                # From config.py
    )
)

# Then run the PPO-trainer (you can comment this out)
asyncio.get_event_loop().run_until_complete(
    PPO_run(
        opponent_class = HeuristicPlayer,
        team = triples,
        tags = ["myTest", "triples"],
        do_log = True, 
        pre_trained="",
        **PPO_PARAMS
    )
)

