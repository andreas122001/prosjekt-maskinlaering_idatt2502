from src.teams import triples, multiple
from src.teambuilders import FirstPickTeambuilder
from stable_baselines3.common.base_class import BaseAlgorithm
from stable_baselines3 import PPO, DQN
from src.environments.test_env import PokeAI
import asyncio
from poke_env.player_configuration import PlayerConfiguration
from poke_env.server_configuration import ServerConfiguration
import argparse
import os
import sys

async def challenge_player(username: str, team: str, algorithm: BaseAlgorithm, model_path: str, 
        bot_config: PlayerConfiguration=PlayerConfiguration("xXPokeAIXx",""), 
        server_config: ServerConfiguration=None):

    env = PokeAI(username, battle_format="gen8ou", team=FirstPickTeambuilder(team),
        player_configuration=bot_config,
        server_configuration=server_config,
        start_challenging=False
    )
    
    agent = algorithm.load(model_path, env)

    env.start_challenging(1)
    print("Waiting for player...")
    state = env.reset()
    done = False
    print("Player accepted!")
    print("Starting battle...")
    while not done:
        action = agent.predict(state)[0]    # Predict action
        state, _, done, _ = env.step(action)

    print("Battle finished.")
    print("Agent won! :)" if env.win_rate == 1 else "Agent lost! :(")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
                        prog = 'challenge_player.py',
                        description = 'Makes AI-agent challenge you')
    parser.add_argument("-u", "--username", type=str, required=True)
    parser.add_argument("-a", "--algorithm", type=str, choices=["PPO", "DQN"], required=False)
    parser.add_argument("-t", "--team", type=str, choices=["triples", "multiple"], required=True)
    parser.add_argument("-p", "--path", type=str, required=True)
    args = parser.parse_args()

    if not args.algorithm:
        algo = os.path.split(os.path.split(args.path)[0])[1].split("_")[0] # Okay, this is spaghetti, but idk how else to do it... It probably doesn't work on Linux or smth like that
        print("Using DQN.")
        if (algo == "dqn"): algo = DQN
        elif (algo == "ppo"): algo = PPO
        else:
            print("Could not determine algorithm from file path, please use '--algorithm' argument.", file=sys.stderr)
            parser.print_help()
            exit()
    else:
        if args.algorithm == "DQN":
            algo = DQN
        elif args.algorithm == "PPO":
            algo = PPO

    if args.team == "triples":
        team = triples
    elif args.team == "multiple":
        team = multiple
    
    asyncio.get_event_loop().run_until_complete(
        challenge_player(
            username=args.username,  # Your username
            team=team,               # The team-type the agent should use
            algorithm=algo,          # The class of the algorithm (stable baselines 3) (DQN or PPO)
            model_path=args.path     # The path for the pre-trained model
        )
    )

