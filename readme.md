# Reinforcement Learning for Pokémon Showdown

![poster](./poster.png)

## Content
- [Reinforcement Learning for Pokémon Showdown](#reinforcement-learning-for-pokémon-showdown)
  - [What this is](#what-this-is)
  - [Our metrics](#our-metrics)
  - [Requirements](#requirements)
    - [Python packages](#python-packages)
    - [Localhost showdown server](#localhost-showdown-server)
  - [To run yourself](#to-run-yourself)
    - [Trainers](#trainers)
    - [Play against yourself](#play-against-yourself)
    - [Use metrics](#use-metrics)



## What this is

This project aim to create an AI-agent for pokémon showdown using the RL-algorithms DQN and PPO.

It uses [poke-env](https://poke-env.readthedocs.io/en/latest/index.html) for the learning environment, [Stable baselines 3](https://stable-baselines3.readthedocs.io/en/master/) for algorithm-implementations and [WandB](https://wandb.ai/) for logging.

## Our metrics

All logged metrics can be found [here](https://wandb.ai/andreas122001/showdown-ai).

The latest tests are tagged as "test3" and "test4".

## Requirements

- Python packages
- Localhost showdown server

### Python packages

To install, run this in your teminal from the project-folder:

```
pip install -r requirements.txt
```

### Localhost showdown server

To run this on your own machine you must download and run you own instance of the pokemon-showdown server. 

An installation guide can be found in the [poke-env-docs](https://poke-env.readthedocs.io/en/latest/getting_started.html#configuring-a-showdown-server). 



## To run yourself

This project contains six runnable scripts:
- main.py               (only for example)
- challenge_player.py   (challenge a player on localhost-server)
- trainer_dqn_sb3.py    (trainer for DQN)
- trainer_ppo_sb3.py    (trainer for PPO)
- trainer_dqn.py        (trainer for our own DQN-implementation, not used anymore)
- evaluate.py           (evaluation script)


### Trainers

If you want to configure the trainers, hyperparamteres can be changes in `config.py` and other parameters can be changed in directly in the script (at the bottom of the files). There is no argument parser in the trainer scripts for configuration.

There are two training-scripts with SB3, one for DQN, `trainer_dqn_sb3.py`, and one for PPO,`trainer_ppo_sb3.py`.

You can either run the trainers as scripts like this from the project folder:

```
python src/trainer_dqn_sb3.py
```

```
python src/trainer_ppo_sb3.py
```

or you can use their *run* method in the *main.py*-file. There is already an example of this in *main.py*.

**NOTE**: If you want to run with logging (using WandB), you should run the scripts as administrator. This is because the WandB/SB3-integration uses symlinks to log files, which on some systems require hightened privileges.

### Play against yourself

To play against yourself you can run the *challenge_player.py*-file as a script. Usage: 

```
challenge_player.py [-h] -u USERNAME [-a {PPO,DQN}] -t {triples,multiple} -p PATH
```

Note that you have to be logged in to the same server to recieve challenges.


### Use metrics

To use your own WandB-account for metrics, copy the `.env.default`-file into `.env` and fill out your API-key in the ´<YOUR_KEY>` field. 

You can also change the project name if you want.

